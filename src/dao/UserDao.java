package dao;

import pojo.User;

/**
 * sql
 * CREATE TABLE user  (
 *   `name` varchar(255) NULL
 * );
 */
public interface UserDao {
    /**
     * 添加用户
     */
    void addUser(User user);
}
