package example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pojo.User;

public class 在Java中获取bean {
    public static void main(String[] args) {
        //创建应用上下文
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        //通过bean的name或class对象获取对应的bean对象
//        User u=context.getBean(User.class);
        User u=(User) context.getBean("user");
        System.out.println(u.getName());
    }
}
