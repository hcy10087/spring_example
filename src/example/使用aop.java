package example;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;

@Aspect//注解为切面
@Controller
public class 使用aop {
    //可以使用@Pointcut将该方法绑定为切点定义方法，以替代繁琐的定义
    @Pointcut("execution(* example.使用aop.AopTest.test(..))")
    public void Pointcut(){}

    //可以通过args(id)的方式获取传入参数
    //前置通知
    @Before("Pointcut()&&args(id)")
    public void before(String id){
        System.out.println("前置通知，传入参数为："+id);
    }

    //定义切点，后置通知
    @After("Pointcut()")
    public void after(){
        System.out.println("后置通知");
    }

    //定义切点，正常执行通知
    @AfterReturning("Pointcut()")
    public void afterReturning(){
        System.out.println("正常执行通知");
    }

    //定义切点，异常结束通知
    @AfterThrowing("Pointcut()")
    public void afterThrowing(){
        System.out.println("异常结束通知");
    }

    //环绕通知
    @Around("Pointcut()")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable{
        try {
            System.out.println("环绕前置通知");
            Object[] objects = joinPoint.getArgs();//获取传入参数
            for (Object o : objects)//打印传入参数
                System.out.println("环绕通知，打印传入参数："+o);
            joinPoint.proceed();//执行下一个通知或目标方法
            System.out.println("环绕后置通知");
            System.out.println("环绕通知正常执行通知");
        }catch (Exception e){
            System.out.println("环绕通知异常结束通知");
        }
    }

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        context.getBean(AopTest.class).test("test");
    }
    @Controller
    /**
     * 测试类
     */
    public class AopTest {
        void test(String id){

        }
    }
}
