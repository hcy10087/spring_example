package example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import pojo.User;

@Controller
public class 指定自动注入的bean {
    //自动注入
    @Autowired
    //指定自动注入的bean的name
    @Qualifier("u")
    User user;
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        System.out.println(context.getBean(指定自动注入的bean.class).user.getName());
    }
}