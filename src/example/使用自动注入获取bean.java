package example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import pojo.User;

@Controller
public class 使用自动注入获取bean {
    //自动注入
    @Autowired
    User user;
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        System.out.println(context.getBean(使用自动注入获取bean.class).user.getName());
    }
}
