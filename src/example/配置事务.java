package example;

import dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import pojo.User;

@Controller
public class 配置事务 {
    @Autowired
    UserDao userDao;
    //发生任何异常都回滚，事务时间超过十秒(该10秒仅为sql执行时间，sleep无效)回滚
    @Transactional(rollbackFor = Exception.class, timeout = 10)
    public void addUser(User user) throws Exception {
        userDao.addUser(user);
//        if(true)
//            throw new Exception();
    }

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring.xml");
        User user=new User();
        user.setName("hh");
        try {
            context.getBean(配置事务.class).addUser(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
